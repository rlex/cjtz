<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>承锦</title>
    <link rel="stylesheet" href="/cjtz/Public/css/foundation.css" />
    <link rel="stylesheet" href="/cjtz/Public/css/style.css" />
    <script src="/cjtz/Public/js/vendor/modernizr.js"></script>

    <style type="text/css">
    .side-nav li:hover{
    background: none repeat scroll 0 0 #F67009;}
    .side-nav li a:not(.button){
      color: white;
    }
    .side-nav li a:not(.button):hover, .side-nav li a:not(.button):focus{
      color: grey;
    }



    </style>
  </head>
  <body>
    
    <div class="row" style="margin-top: 10px;">
      <div class="large-3 columns">
        <img width="241" height="43" src="/cjtz/Public/img/logo.png" alt="承锦之信  锦众之业">
      </div>
      <div class="large-9 columns" style="font-size: 50px;
color: rgb(255,133,14);text-align: center;
height: 144px;line-height: 144px;font-weight: bold;">
         承锦之信&nbsp;&nbsp;&nbsp;锦众之业

      </div>
      
    </div>
      <div class="mynavbarbg">
        <div class="row">
          <nav class="top-bar" data-topbar role="navigation" style="color:#FF850E">
            <ul class="title-area"></ul>
            <section class="top-bar-section">
              <!-- Right Nav Section -->      
              <ul class="right">
              </ul>
              <!-- Left Nav Section -->      
              <ul class="left">
                <li class="active">
                  <a href="#">首页</a>
                </li>
                <li class="active">
                  <a href="#">承锦介绍</a>
                </li>
                <li class="active">
                  <a href="#" class="ui">联系我们 </a>
                </li>
              </ul>
            </section>
          </nav>
        </div>
      </div>
  
<div class="intro inner">
  <div class="row">
    <div class="large-2 columns" style="background-color: rgb(255,133,14);">
      <ul class="side-nav">
           <li><a href="#">公司介绍</a></li>
            <li><a href="#">业务介绍</a></li>
            <li><a href="#">承锦投资优势</a></li>
           <li><a href="#">承锦投资业绩</a></li>
           <li><a href="#">承锦控股未来</a></li>
           
      </ul>
    </div>
    <div class="large-9 columns">
      
      <div class="info" style="background-color: red;width:80%;margin: 0 auto;">

      
<p>
承锦控股有限公司，是一家注册资金亿元的互联网金融公司，主营业务为新型的阳光理财P2P服务：承锦投资。专业的技术、律师、分析团队，合法合规的理财合同以及将投资者利益放在首位的服务理念是我们理财服务运营的基础，我们秉承着“承己之信，锦众之业”的创业宗旨，致力于成为您财富稳定增值的管理者。</p>



      </div>
    </div>
  </div>
</div>


  <br>
  <hr>

    
      <div class="foot" id="footer">
      <br>
      <div class="row">
        <div class="row property">
          <div class="medium-4 columns">
            <div class="property-info c_l"></div>
          </div>
          <div class="medium-8 columns">
            <div class="row collapse">
              <div class="medium-4 columns">
                <div class="" style="text-align: center;">
                  <h4 class="hide-for-small">Want more?</h4>
                  <ul>
                    <li>
                      <a href="business/services.html">Foundation Business</a>
                    </li>
                    <li>
                      <a href="http://zurb.com/responsive">Responsive</a>
                    </li>
                    <li>
                      <a href="http://zurb.com/apps">Design Apps</a>
                    </li>
                    <li>
                      <a href="learn/training.html">Foundation Training</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="medium-4 columns">
                <div class="" style="text-align: center;">
                  <h4 class="hide-for-small">Talk to us</h4>
                  <p>
                    Tweet us at
                    <br>      
                    <a href="https://twitter.com/zurbfoundation">@ZURBfoundation</a>
                  </p>
                  <p>
                    Or check our
                    <br>      
                    <a href="support/support.html">support page</a>
                  </p>
                </div>
              </div>
              <div class="medium-4 columns">
                <div class="" style="text-align: center;">
                  <h4 class="hide-for-small">二维码</h4>
                  <a class="" href="http://zurb.com/news"><img width="80" height="80" src="/cjtz/Public/img/ewm.jpg" alt="承锦投资二维码"></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="copyright warpper">
          <p>版权所有 &copy;平安银行股份有限公司 未经许可不得复制、转载或摘编，违者必究!</p>
          <p>Copyright &copy; PingAn Bank Co., Ltd. All Rights Reserved</p>
          <p>
            <a href="http://www.sznet110.gov.cn/index.jsp" target="_blank">
              <img width="16" height="23" border="0" alt="" src="http://bank.pingan.com/app_images/pingan/v30/newbank/icon_02.gif"></a>
            <a href="http://www.sznet110.gov.cn/webrecord/innernet/Welcome.jsp?bano=440310120110" target="_blank">
              <img width="15" height="20" border="0" alt="" src="http://bank.pingan.com/app_images/pingan/v30/newbank/icon_01.gif"></a>
            ICP许可证号
            <a title="粤ICP备06118290号-12" target="_blank" href="http://www.miitbeian.gov.cn/">粤ICP备06118290号-12</a>
          </p>
        </div>
      </div>
    </div>
    
    <script src="/cjtz/Public/js/vendor/jquery.js"></script>
    /*<script src="/cjtz/Public/js/foundation.min.js"></script>*/
<!-- <script src="/cjtz/Public/js/foundation/foundation4.js"></script>
<script src="/cjtz/Public/js/foundation/foundation.section.js"></script> -->
    <script>
      $(document).foundation();
    </script>
  </body>
</html>