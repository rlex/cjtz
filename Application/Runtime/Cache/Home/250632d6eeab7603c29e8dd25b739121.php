<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>承锦投资</title>
    <link rel="stylesheet" href="/cjtz/Public/css/foundation.css" />
    <link rel="stylesheet" href="/cjtz/Public/css/style.css" />
    <script src="/cjtz/Public/js/vendor/modernizr.js"></script>

    <style type="text/css">
    .side-nav li:hover{
    background: none repeat scroll 0 0 #F67009;}
    .side-nav li a:not(.button){
      color: white;
    }
    .side-nav li a:not(.button):hover, .side-nav li a:not(.button):focus{
      color: grey;
    }



    </style>
        <style type="text/css">
    .bottm_div{
      width:65%;
      margin: 0 auto;
    }
  .bottm_div p{
    font-size: 12px;
  }
    </style>
  </head>
  <body>
    
    <div class="row" style="margin-top: 10px;">
      <div class="large-3 columns">
        <img width="241" height="43" src="/cjtz/Public/img/logo.png" alt="承锦之信  锦众之业">
      </div>
      <div class="large-9 columns" style="font-size: 50px;
color: rgb(255,133,14);text-align: center;
height: 144px;line-height: 144px;font-weight: bold;">
         承锦之信&nbsp;&nbsp;&nbsp;锦众之业

      </div>
      
    </div>
      <div class="mynavbarbg">
        <div class="row">
          <nav class="top-bar" data-topbar role="navigation" style="color:#FF850E">
            <ul class="title-area"></ul>
            <section class="top-bar-section">
              <!-- Right Nav Section -->      
              <ul class="right">
              </ul>
              <!-- Left Nav Section -->      
               <ul class="left">
                <li class="active">
                  <a href='<?php echo U("Home/Index/index");?>'>首页</a>
                </li>
                <li class="active">
                  <a href='<?php echo U("Home/Index/introduction");?>'>承锦介绍</a>
                </li>
                <li class="active">
                  <a href='<?php echo U("Home/Index/contact");?>' class="ui">联系我们 </a>
                </li>
              </ul>
            </section>
          </nav>
        </div>
      </div>
  
<div class="intro inner">
  <div class="row">
    <div class="large-2 columns" style="background-color: rgb(255,133,14);">
      <ul class="side-nav" id="in_title">
           <li><a href="#" class="0">公司介绍</a></li>
            <li><a href="#" class="1">业务介绍</a></li>
            <li><a href="#" class="2">承锦投资优势</a></li>
           <li><a href="#" class="3">承锦投资业绩</a></li>
           <li><a href="#" class="4">承锦控股未来</a></li>
      </ul>
    </div>
    <div class="large-9 columns">
        <div id="intro_0" >
            <?php echo ($com_intro["content"]); ?>
        </div>
        <div id="intro_1" >
            <?php echo ($business_intro["content"]); ?>
        </div>
        <div id="intro_2" >
            <?php echo ($investment_advantage["content"]); ?>
        </div>
        <div id="intro_3" >
            <?php echo ($investment_achievement["content"]); ?>jeje
        </div>
        <div id="intro_4">
            <?php echo ($holding_future["content"]); ?>heheh
        </div>
    </div>
  </div>
</div>


  <br>
  <hr>
       <div class="foot" id="footer">
      <br>
      <div class="row">
        <div class="row property">
          
          <div class="medium-12 columns">
            <div class="row collapse">
              <div class="medium-4 columns">
                <div class="" style="text-align: center;">
                  <h4 class="hide-for-small">公司地址</h4>
                  <div class="bottm_div" style="text-align:left;font-size: 12px;">
                  <p>
                   西城区办公地点：<br>
                   北京市西城区新街口北路3号星街坊大厦509<br></p>
                   <p>昌平区办公地点：<br>
                   北京市昌平区超前路37号智慧中心十号楼二层<br>
                  </p>
                  </div>
                </div>
              </div>
              <div class="medium-4 columns">
                <div class="" style="text-align: center;">
                  <h4 class="hide-for-small">联系我们</h4>
                   <div class="bottm_div" style="text-align:left;font-size: 12px;">
                  <p>
                    西城区办公地点：010-82270091转869
                    <br>      
                  </p>
                  <p>
                    昌平区办公地点：010-82185397
                    <br>      
                  </p>
                  <p>
                    企业客服QQ：800032165
                    <br>      
                  </p>
                  <p>
                    邮箱：18210681068@163.com
                    <br>      
                  </p>
                  </div>
                </div>
              </div>
              <div class="medium-4 columns">
                <div class="" style="text-align: center;">
                  <h4 class="hide-for-small">二维码</h4>
                  <a class="" href="http://zurb.com/news"><img width="80" height="80" src="/cjtz/Public/img/ewm.jpg" alt="承锦投资二维码"></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="copyright warpper">
          <p>版权所有 &copy;承锦投资 未经许可不得复制、转载或摘编，违者必究!</p>
          <p>
           
          </p>
        </div>
      </div>
    </div>
    <script src="/cjtz/Public/js/vendor/jquery.js"></script>
    <script src="/cjtz/Public/js/foundation.min.js"></script>
<!-- <script src="/cjtz/Public/js/foundation/foundation4.js"></script>
<script src="/cjtz/Public/js/foundation/foundation.section.js"></script> -->
    <script>
      $(document).foundation();
      $(document).ready(function(){
          $("#intro_4").hide();
          $("#intro_3").hide();
          $("#intro_2").hide();
          $("#intro_1").hide();
          $('#in_title a').click(function (e) {
              var index = $(this).attr("class");
              for(var i = 0 ; i<5;i++){
                if(index==i){
                    $("#intro_"+index).show();
                }else{
                    $("#intro_"+i).hide();
                }
              }
          })
      });
    </script>
  </body>
</html>