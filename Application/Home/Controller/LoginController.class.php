<?php
/**
 * Created by PhpStorm.
 * User: Qiangguo
 * Date: 14-10-29
 * Time: 下午10:38
 */
namespace Home\Controller;
use Think\Controller;
class LoginController extends Controller {
    public function index(){
        $this->display('Back:login');
    }
    public function loginRedirect(){
        $this->display('Back:login');
        /*        $this->display(T('Login/login'));*/
    }

    public function redirectPhp(){
        echo 'redirect,thinkphp!';
        redirect('/Home/Login/index', 1, '用户不存在...');
    }
    public function logout(){
        unset($_SESSION['user']);
        $this->loginRedirect();
    }
    public function loginAction(){
        $params = $_POST;
        if(!$params){
            return;
        }
        $Dao = M("User");
        // 查询数据
        $list=$Dao->where("userid= '%s'",$params['userid'])->select();
        if(count($list)==0){
            $this->redirectPhp();
        }elseif(count($list)==1){
            if($list[0]["pwd"]!=$params["password"]){
                redirect('/Home/Login/index', 1, '密码错误...');
            }
            session('user',$list[0]);  //设置session
            // 输出模板
            $this->getAllNews();
        }
        // 模板变量赋值
        // 输出模板
        /* $this->display();*/
    }
    public function getNews($id){
        $Dao = M("News");
        $map['newsid'] = $id;
        // 查询数据
        $list=$Dao->where($map)->find();
        $this->assign('news',$list);
        /*   session('user',$list[0]);  //设置session*/
        // 输出模板
        $this->display('Back:detail');
    }
    public function getContact($id){
        $Dao = M("Feedback");
        $map['id'] = $id;
        // 查询数据
        $list=$Dao->where($map)->find();
        $this->assign('feedback',$list);
        /*   session('user',$list[0]);  //设置session*/
        // 输出模板
        $this->display('Back:feedbackDetail');
    }
    public function deleteContact($id){
        if($_SESSION('user')==null)
            redirect('/Home/Login/index', 1, '木有权限删除...');
        $Dao = M("Feedback");
        $map['id'] = $id;
        // 查询数据
        $Dao->where($map)->delete();
        $this->getAllContact();
    }
    public function getAllContact(){
        $Dao = M("Feedback");
        // 查询数据
        $list=$Dao->order('time desc')->select();
        $this->assign('feedbacklist',$list);
        $this->display('Back:feedbackList');
    }
    public function getAllNews(){
        $Dao = M("News");
            // 查询数据
            $list=$Dao->order('time desc')->select();
            $this->assign('newslist',$list);
            $this->display('Back:newsList');
    }
    public function addNews(){
        $this->display('Back:addNews');
    }
    public function deleteNews($id){
        if($_SESSION('user')==null)
            redirect('/Home/Login/index', 1, '木有权限删除...');
        $Dao = M("News");
        $map['newsid'] = $id;
        // 查询数据
        $Dao->where($map)->delete();
        $this->getAllNews();
    }
    public function addNewsDetail(){
        $News = M("News"); // 实例化User对象
        $data['content']=$_POST['editor1'];
        $data['title']=$_POST['acTitle'];
        $data['time']=date('Y-m-d',time());
        $data['tag']=$_POST['tag'];
        $News->add($data);
        $this->getAllNews();
    }
    //改变Ueditor 默认图片上传路径
    public function checkPic(){
        import('ORG.Net.UploadFile');
        $upload = new UploadFile();// 实例化上传类
        $upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->autoSub =true ;
        $upload->subType ='date' ;
        $upload->dateFormat ='ym' ;
        $upload->savePath =  './Uploads/thumb/';// 设置附件上传目录
        if($upload->upload()){
            $info =  $upload->getUploadFileInfo();
            echo json_encode(array(
                'url'=>$info[0]['savename'],
                'title'=>htmlspecialchars($_POST['pictitle'], ENT_QUOTES),
                'original'=>$info[0]['name'],
                'state'=>'SUCCESS'
            ));
        }else{
            echo json_encode(array(
                'state'=>$upload->getErrorMsg()
            ));
        }
    }

}