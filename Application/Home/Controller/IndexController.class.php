<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function index(){
        //echo "aaa";
        //exit(0);
        $this->toMain();
    }
    public function toMain(){
        $Dao = M("News");
        // 查询数据
        $map['tag'] = "最新消息";
        $list=$Dao->where($map)->order('time desc')->limit(8)->select();
        $this->assign('latest_newslist',$list);
        $map['tag'] = "今日运营情况";
        $list2=$Dao->where($map)->order('time desc')->limit(8)->select();
        $this->assign('today_operation',$list2);
     /*   session('user',$list[0]);  //设置session*/
        // 输出模板
        $this->display('Front:index');
    }
    public function addFeedbackDetail(){
        $Feedback = M("Feedback"); // 实例化User对象
        $data['title']=$_POST['title'];
        $data['content']=$_POST['content'];
        $data['time']=date('Y-m-d',time());
        $Feedback->add($data);

        $this->contact();
    }

    public function getNews($id){
        $Dao = M("News");
        $map['newsid'] = $id;
        // 查询数据
        $list=$Dao->where($map)->find();
        $this->assign('news',$list);
        /*   session('user',$list[0]);  //设置session*/
        // 输出模板
        $this->display('Front:front_detail');
    }
    public function more(){
        $Dao = M("News");
        // 查询数据
        $list=$Dao->order('time desc')->select();
        $this->assign('newslist',$list);
        /*   session('user',$list[0]);  //设置session*/
        // 输出模板
        $this->display('Front:front_news');
    }
    public function hello(){
        $data['name'] = 'hahahahahahah';
        $data['email'] = 'thinkphp@qq.com';
        $this->assign('data',$data);

        $this->display('Front:main');
        /*        $this->display(T('Login/login'));*/
    }
   	public function introduction(){
   		$Dao = M("News");
        // 查询数据
        $map['tag'] = "公司介绍";
        $list=$Dao->where($map)->order('time desc')->find();
        $this->assign('com_intro',$list);
        $map['tag'] = "业务介绍";
        $list=$Dao->where($map)->order('time desc')->find();
        $this->assign('business_intro',$list);
        $map['tag'] = "投资优势";
        $list=$Dao->where($map)->order('time desc')->find();
        $this->assign('investment_advantage',$list);
        $map['tag'] = "投资业绩";
        $list=$Dao->where($map)->order('time desc')->find();
        $this->assign('investment_achievement',$list);
        $map['tag'] = "控股未来";
        $list=$Dao->where($map)->order('time desc')->find();
        $this->assign('holding_future',$list);
        $this->display('Front:introduction');
   	}
    public function gsjs(){
        $Dao = M("News");
        // 查询数据
        $map['tag'] = "公司介绍";
        $list=$Dao->where($map)->order('time desc')->find();
        $this->assign('com_intro',$list);
        $map['tag'] = "业务介绍";
        $list=$Dao->where($map)->order('time desc')->find();
        $this->assign('business_intro',$list);
        $map['tag'] = "投资优势";
        $list=$Dao->where($map)->order('time desc')->find();
        $this->assign('investment_advantage',$list);
        $map['tag'] = "投资业绩";
        $list=$Dao->where($map)->order('time desc')->find();
        $this->assign('investment_achievement',$list);
        $map['tag'] = "控股未来";
        $list=$Dao->where($map)->order('time desc')->find();
        $this->assign('holding_future',$list);
        $this->display('Front:introduction');
    }
    public function contact(){
        $Dao = M("News");
        // 查询数据
        $map['tag'] = "联系我们";
        $list=$Dao->where($map)->order('time desc')->find();
        $this->assign('contact_us',$list);
        $map['tag'] = "公司地址";
        $list=$Dao->where($map)->order('time desc')->find();
        $this->assign('comp_addr',$list);
        $this->display('Front:contact');
    }

}